

enum AuthError {
    WRONG_CREDENTIALS,
    SERVER_FAIL,
    EXPIRED_SESSION,
    UNEXPECTED_ERROR
}

//console.log(AuthError[AuthError.WRONG_CREDENTIALS])

enum AuthError2{
    WRONG_CREDENTIALS = 'wrong credentials',
    SERVER_FAIL= 'server fail',
    EXPIRED_SESSION= 'expired session'
}

function handleError(error:AuthError){
    switch(error){
        case AuthError.EXPIRED_SESSION:
            console.log('Restart the Session')
            break;
        case AuthError.SERVER_FAIL:
            console.log('Restart the Server')
            break;
        case AuthError.WRONG_CREDENTIALS:
            console.log('provide the correct credentials')
            break;
        case AuthError.UNEXPECTED_ERROR:
            console.log('check your input')
            break;
        default:
            break;
    
    }

}

handleError(AuthError.UNEXPECTED_ERROR)