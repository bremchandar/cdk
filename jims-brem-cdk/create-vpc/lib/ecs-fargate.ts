import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as ecs from 'aws-cdk-lib/aws-ecs';
import * as cdk from 'aws-cdk-lib';
import * as ecr from 'aws-cdk-lib/aws-ecr'
import { Construct } from 'constructs';

export class MicroservicesEcsStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const repositoryService1 = new cdk.aws_ecr.Repository(this, 'Service1Repo', {
      imageScanOnPush: true,
    });

    const repositoryService2 = new cdk.aws_ecr.Repository(this, 'Service2Repo', {
      imageScanOnPush: true,
    });

    // Create VPC and Fargate Cluster
    // NOTE: Limit AZs to avoid reaching resource quotas
    const vpc = new ec2.Vpc(this, 'MicroserviceVPC', { maxAzs: 2 });
    
    const cluster = new ecs.Cluster(this, 'MicroserviceFargateCluster', {
      vpc,
      enableFargateCapacityProviders: true,
    });

    // Service1 task definition and service
    const service1TaskDefinition = new ecs.FargateTaskDefinition(this, 'Service1TaskDef');
    service1TaskDefinition.addContainer('service1', {
      //image: ecs.ContainerImage.fromEcrRepository(repositoryService1),
      image: ecs.ContainerImage.fromRegistry("coderaiser/cloudcmd"),

    });

    new ecs.FargateService(this, 'FargateService1', {
      cluster,
      taskDefinition: service1TaskDefinition
    });


    // Service2 task definition and service
    const service2TaskDefinition = new ecs.FargateTaskDefinition(this, 'Service2TaskDef');

    service2TaskDefinition.addContainer('service2', {
      //image: ecs.ContainerImage.fromEcrRepository(repositoryService2),
      image: ecs.ContainerImage.fromRegistry("coderaiser/cloudcmd"),

    });

    new ecs.FargateService(this, 'FargateService2', {
      cluster,
      taskDefinition: service2TaskDefinition
    });

  }
}