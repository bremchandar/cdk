#!/bin/bash

#set -x
# This script generates AWS Programmatic Access credentials from a user authenticated via SSO
# Before using, make sure that the AWS SSO is configured in your CLI: `aws configure sso`

#profile=${AWS_PROFILE-default}
profile=jims-sandbox-admin
temp_identity=$(aws --profile "$profile" sts get-caller-identity)
# run a command to populate the cache
run_command=$(aws s3 ls --profile "$profile")

for  i in "$@" ;do
	case $i in 
		-new)
			rm ~/.aws/credentials
  			aws sso login --profile "$profile"
			aws s3 ls
			credentials=$(cat ~/.aws/cli/cache/*.json)
			echo credentials is $credentials
			;;
		-old)
			credentials=$(cat ~/.aws/cli/cache/*.json)
			echo credentials is $credentials
			;;
		esac
	done


echo "=> updating ~/.aws/credentials as profile $profile"

access_key_id=`echo $credentials | awk '{print $5}'|sed -e 's/,//'|sed -e 's/\\"//g'`
secret_access_key=`echo $credentials |  awk '{print $7}'|sed -e 's/,//'|sed -e 's/\\"//g'`
session_token=`echo $credentials |  awk '{print $9}'|sed -e 's/,//'|sed -e 's/\\"//g'`

echo $access_key_id

aws configure set --profile "$profile" aws_access_key_id $access_key_id
aws configure set --profile "$profile" aws_secret_access_key $secret_access_key
aws configure set --profile "$profile" aws_session_token $session_token

aws configure set --profile "default" aws_access_key_id "$access_key_id"
aws configure set --profile "default" aws_secret_access_key "$secret_access_key"
aws configure set --profile "default" aws_session_token "$session_token"

echo "[OK] done"
