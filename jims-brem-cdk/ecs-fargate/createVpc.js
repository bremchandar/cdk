"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import ecs = require('aws-cdk-lib/aws-ecs');
//import ecs_patterns = require('aws-cdk-lib/aws-ecs-patterns');
const ec2 = require("aws-cdk-lib/aws-ec2");
const cdk = require("aws-cdk-lib");
//import * as logs from '@aws-cdk/aws-logs';
class vpcStack extends cdk.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);
        // Create a VPC
        const vpc = new ec2.Vpc(this, 'my-cdk-vpc', {
            cidr: '10.20.0.0/16',
            natGateways: 1,
            maxAzs: 3,
            subnetConfiguration: [
                {
                    name: 'Application-subnet',
                    subnetType: ec2.SubnetType.PRIVATE_WITH_NAT,
                    cidrMask: 24,
                },
                {
                    name: 'public-subnet-1',
                    subnetType: ec2.SubnetType.PUBLIC,
                    cidrMask: 24,
                },
                {
                    name: 'DB-subnet',
                    subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
                    cidrMask: 28,
                },
            ],
        });
        // 👇 update the Name tag for the VPC
        cdk.Aspects.of(vpc).add(new cdk.Tag('Name', 'my-cdk-vpc'));
        vpc.addFlowLog('FlowlLogCloudWatch', {
            trafficType: ec2.FlowLogTrafficType.ALL
        });
    }
}
const app = new cdk.App();
new vpcStack(app, 'vpc-stack');
app.synth();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlVnBjLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY3JlYXRlVnBjLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsOENBQThDO0FBQzlDLGdFQUFnRTtBQUNoRSwyQ0FBNEM7QUFDNUMsbUNBQW9DO0FBQ3BDLDRDQUE0QztBQUs1QyxNQUFNLFFBQVMsU0FBUSxHQUFHLENBQUMsS0FBSztJQUM5QixZQUFZLEtBQWMsRUFBRSxFQUFVLEVBQUUsS0FBc0I7UUFDNUQsS0FBSyxDQUFDLEtBQUssRUFBRSxFQUFFLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFeEIsZUFBZTtRQUdmLE1BQU0sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsWUFBWSxFQUFFO1lBQzFDLElBQUksRUFBRSxjQUFjO1lBQ3BCLFdBQVcsRUFBRSxDQUFDO1lBQ2QsTUFBTSxFQUFFLENBQUM7WUFDVCxtQkFBbUIsRUFBRTtnQkFDbkI7b0JBQ0UsSUFBSSxFQUFFLG9CQUFvQjtvQkFDMUIsVUFBVSxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCO29CQUMzQyxRQUFRLEVBQUUsRUFBRTtpQkFDYjtnQkFDRDtvQkFDRSxJQUFJLEVBQUUsaUJBQWlCO29CQUN2QixVQUFVLEVBQUUsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNO29CQUNqQyxRQUFRLEVBQUUsRUFBRTtpQkFDYjtnQkFDRDtvQkFDRSxJQUFJLEVBQUUsV0FBVztvQkFDakIsVUFBVSxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCO29CQUMzQyxRQUFRLEVBQUUsRUFBRTtpQkFDYjthQUNGO1NBQ0YsQ0FBQyxDQUFDO1FBRUgscUNBQXFDO1FBQ3JDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFFM0QsR0FBRyxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRTtZQUNuQyxXQUFXLEVBQUUsR0FBRyxDQUFDLGtCQUFrQixDQUFDLEdBQUc7U0FDeEMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztDQUNGO0FBSUQsTUFBTSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7QUFFMUIsSUFBSSxRQUFRLENBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0FBRWhDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8vaW1wb3J0IGVjcyA9IHJlcXVpcmUoJ2F3cy1jZGstbGliL2F3cy1lY3MnKTtcbi8vaW1wb3J0IGVjc19wYXR0ZXJucyA9IHJlcXVpcmUoJ2F3cy1jZGstbGliL2F3cy1lY3MtcGF0dGVybnMnKTtcbmltcG9ydCBlYzIgPSByZXF1aXJlKCdhd3MtY2RrLWxpYi9hd3MtZWMyJyk7XG5pbXBvcnQgY2RrID0gcmVxdWlyZSgnYXdzLWNkay1saWInKTtcbi8vaW1wb3J0ICogYXMgbG9ncyBmcm9tICdAYXdzLWNkay9hd3MtbG9ncyc7XG5cblxuXG5cbmNsYXNzIHZwY1N0YWNrIGV4dGVuZHMgY2RrLlN0YWNrIHtcbiAgY29uc3RydWN0b3Ioc2NvcGU6IGNkay5BcHAsIGlkOiBzdHJpbmcsIHByb3BzPzogY2RrLlN0YWNrUHJvcHMpIHtcbiAgICBzdXBlcihzY29wZSwgaWQsIHByb3BzKTtcblxuICAgIC8vIENyZWF0ZSBhIFZQQ1xuXG4gICAgXG4gICAgY29uc3QgdnBjID0gbmV3IGVjMi5WcGModGhpcywgJ215LWNkay12cGMnLCB7XG4gICAgICBjaWRyOiAnMTAuMjAuMC4wLzE2JyxcbiAgICAgIG5hdEdhdGV3YXlzOiAxLFxuICAgICAgbWF4QXpzOiAzLFxuICAgICAgc3VibmV0Q29uZmlndXJhdGlvbjogW1xuICAgICAgICB7XG4gICAgICAgICAgbmFtZTogJ0FwcGxpY2F0aW9uLXN1Ym5ldCcsXG4gICAgICAgICAgc3VibmV0VHlwZTogZWMyLlN1Ym5ldFR5cGUuUFJJVkFURV9XSVRIX05BVCxcbiAgICAgICAgICBjaWRyTWFzazogMjQsXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBuYW1lOiAncHVibGljLXN1Ym5ldC0xJyxcbiAgICAgICAgICBzdWJuZXRUeXBlOiBlYzIuU3VibmV0VHlwZS5QVUJMSUMsXG4gICAgICAgICAgY2lkck1hc2s6IDI0LFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgbmFtZTogJ0RCLXN1Ym5ldCcsXG4gICAgICAgICAgc3VibmV0VHlwZTogZWMyLlN1Ym5ldFR5cGUuUFJJVkFURV9JU09MQVRFRCxcbiAgICAgICAgICBjaWRyTWFzazogMjgsXG4gICAgICAgIH0sXG4gICAgICBdLFxuICAgIH0pO1xuXG4gICAgLy8g8J+RhyB1cGRhdGUgdGhlIE5hbWUgdGFnIGZvciB0aGUgVlBDXG4gICAgY2RrLkFzcGVjdHMub2YodnBjKS5hZGQobmV3IGNkay5UYWcoJ05hbWUnLCAnbXktY2RrLXZwYycpKTtcblxuICAgIHZwYy5hZGRGbG93TG9nKCdGbG93bExvZ0Nsb3VkV2F0Y2gnLCB7XG4gICAgICB0cmFmZmljVHlwZTogZWMyLkZsb3dMb2dUcmFmZmljVHlwZS5BTExcbiAgICB9KTtcbiAgXG4gIH1cbn1cblxuXG5cbmNvbnN0IGFwcCA9IG5ldyBjZGsuQXBwKCk7XG5cbm5ldyB2cGNTdGFjayAoYXBwLCAndnBjLXN0YWNrJyk7XG5cbmFwcC5zeW50aCgpO1xuIl19