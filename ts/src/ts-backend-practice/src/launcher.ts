import {Server} from "./server"

class Launcher {
    private server: Server = new Server()

    public launchApp(){
        this.server.startServer();
    }

}

new Launcher().launchApp();
