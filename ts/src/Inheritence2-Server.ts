
interface Iserver{
    startServer(): void
    stopServer(): void
}

 class Server2  implements Iserver{

    public port: number
    public address: string

    constructor(port:number, address:string){
        this.port = port;
        this.address=address;
    
    }

    async startServer() {
        const data = await this.getData();
        console.log(`Starting Server at: ${this.port}: ${this.address}`)
    }

     stopServer() : void {}

         async getData(): Promise<string>{
             return '{}'
         }
     }


const someServer2: Iserver = new Server2(8080,'localhost');
someServer2.startServer();
someServer2.stopServer();

