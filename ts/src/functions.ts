interface Person {
    firstName: String,
    lastName: String,
    job?: job,
    isVisitor?: Boolean
}

type job = 'Engineer' | 'Programmer'

function generateEmail1(input:Person, force?:boolean) {
    if(input.isVisitor && !force ){
        return undefined
    }else {
    return `${input.firstName}.${input.lastName}@gmail.com`
    }
}

console.log(generateEmail1({
    firstName: 'Brem',
    lastName: 'Chandru',
    isVisitor: true
},true ))