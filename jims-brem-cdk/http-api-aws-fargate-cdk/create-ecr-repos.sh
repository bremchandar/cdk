#!/usr/bin/bash
#set -x
echo script name: $0
echo $# arguments
if [ "$#" -ne 3 ];
	then echo "illegal number of arguments. Please pass AWS profile,account number and region"
		exit 1
fi
aws_profile=${1}
aws_account=${2}
aws_region=${3}
echo aws_profile is $aws_profile
echo aws_account is ${aws_account}
echo aws_region is ${aws_region}
#exit 0

## Build the docker Images
cd /c/Users/brem.chandru/Documents/cdk/jims-brem-cdk/http-api-aws-fargate-cdk/src/author-service
npm install --save
docker build -t author-service .
docker tag author-service:latest \
	${aws_account}.dkr.ecr.${aws_region}.amazonaws.com/author-service:latest

cd /c/Users/brem.chandru/Documents/cdk/jims-brem-cdk/http-api-aws-fargate-cdk/src/book-service
npm install --save
docker build -t book-service .
docker tag book-service:latest \
	${aws_account}.dkr.ecr.${aws_region}.amazonaws.com/book-service1:latest

aws --profile  ${aws_profile} ecr get-login-password --region ${aws_region} | docker login  --username AWS   --password-stdin ${aws_account}.dkr.ecr.${aws_region}.amazonaws.com

aws --profile  ${aws_profile} ecr create-repository \
            --repository-name book-service1 \
            --image-scanning-configuration scanOnPush=false \
            --region ${aws_region}

aws --profile  ${aws_profile} ecr create-repository \
            --repository-name author-service \
            --image-scanning-configuration scanOnPush=false \
            --region ${aws_region}

## push the repos to ecr
cd /c/Users/brem.chandru/Documents/cdk/jims-brem-cdk/http-api-aws-fargate-cdk/src/book-service
docker push ${aws_account}.dkr.ecr.${aws_region}.amazonaws.com/book-service1:latest
cd /c/Users/brem.chandru/Documents/cdk/jims-brem-cdk/http-api-aws-fargate-cdk/src/author-service
docker push ${aws_account}.dkr.ecr.${aws_region}.amazonaws.com/author-service:latest
