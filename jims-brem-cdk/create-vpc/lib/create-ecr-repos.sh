#!/usr/bin/bash
set -x
echo script name: $0
echo $# arguments
if [ "$#" -ne 1 ];
	then echo "illegal number of arguments. Please pass AWS account number"
		exit 1
fi
aws_account=${1}
echo aws_account is ${aws_account}
#exit 0

## Build the docker Images
cd /c/Users/brem.chandru/Documents/cdk/jims-brem-cdk/http-api-aws-fargate-cdk/src/author-service
npm install --save
docker build -t author-service .
docker tag author-service:latest \
	${aws_account}.dkr.ecr.us-west-2.amazonaws.com/author-service:latest

cd /c/Users/brem.chandru/Documents/cdk/jims-brem-cdk/http-api-aws-fargate-cdk/src/book-service
npm install --save
docker build -t book-service .
docker tag book-service:latest \
	${aws_account}.dkr.ecr.us-west-2.amazonaws.com/book-service1:latest

aws ecr get-login-password --region us-west-2 | docker login  --username AWS   --password-stdin ${aws_account}.dkr.ecr.us-west-2.amazonaws.com

aws ecr create-repository \
            --repository-name book-service1 \
            --image-scanning-configuration scanOnPush=false \
            --region us-west-2

aws ecr create-repository \
            --repository-name author-service \
            --image-scanning-configuration scanOnPush=false \
            --region us-west-2

## push the repos to ecr
cd /c/Users/brem.chandru/Documents/cdk/jims-brem-cdk/http-api-aws-fargate-cdk/src/book-service
docker push ${aws_account}.dkr.ecr.us-west-2.amazonaws.com/book-service1:latest
cd /c/Users/brem.chandru/Documents/cdk/jims:-brem-cdk/http-api-aws-fargate-cdk/src/author-service
docker push ${aws_account}.dkr.ecr.us-west-2.amazonaws.com/author-service:latest

