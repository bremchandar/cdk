#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { FargateVpclinkStack } from '../lib/fargate-vpclink-stack';
import { HttpApiStack } from '../lib/httpApi-stack';

const jimsSandbox = { account:'587759737452', region: 'ap-southeast-2' };
const BremPersonal = { account:'798616169137', region: 'us-east-1' };

const app = new cdk.App();
const fargateVpclinkStack = new FargateVpclinkStack(app, 'FargateVpclinkStack', { env: jimsSandbox });
new HttpApiStack(app, 'HttpApiStack', fargateVpclinkStack.httpVpcLink, fargateVpclinkStack.httpApiListener , { env: jimsSandbox });

const BremVPClinkstack = new FargateVpclinkStack(app, 'FargateVpclinkStack-BremPersonal', { env: BremPersonal });
new HttpApiStack(app, 'HttpApiStack-BremPersonal', BremVPClinkstack.httpVpcLink, BremVPClinkstack.httpApiListener , { env: BremPersonal });
