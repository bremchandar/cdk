import ec2 = require('aws-cdk-lib/aws-ec2');
import cdk = require('aws-cdk-lib');
import { Bucket } from 'aws-cdk-lib/aws-s3';
import { CfnOutput, CfnParameter, Duration } from 'aws-cdk-lib';
import { CfnOutcome } from 'aws-cdk-lib/aws-frauddetector';


export class s3Stack extends cdk.Stack {
    constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
      super(scope, id, props);
      

      const duration = new CfnParameter(this,'duration',{
          type: 'Number',
          default:6,
          minValue:1,
          maxValue:10
      })

      const myBucket = new Bucket(this,'somebucket',{
        lifecycleRules:[
          {
            expiration: Duration.days(duration.valueAsNumber)
          }
        ]
      }
      )
  
      new CfnOutput(this,'mybucket',{
        value: myBucket.bucketName
      }
      )
  
      
    }
  }
  
  
  const app = new cdk.App();
  new s3Stack(app, 's3-stack');
  app.synth();