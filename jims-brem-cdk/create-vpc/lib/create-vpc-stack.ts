//import ecs = require('aws-cdk-lib/aws-ecs');
//import ecs_patterns = require('aws-cdk-lib/aws-ecs-patterns');
import ec2 = require('aws-cdk-lib/aws-ec2');
import cdk = require('aws-cdk-lib');

//import * as logs from '@aws-cdk/aws-logs';




export class vpcStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create a VPC

    
    const vpc = new ec2.Vpc(this, 'my-cdk-vpc', {
      cidr: '10.20.0.0/16',
      natGateways: 1,
      maxAzs: 3,
      subnetConfiguration: [
        {
          name: 'Application-subnet',
          subnetType: ec2.SubnetType.PRIVATE_WITH_NAT,
          cidrMask: 24,
        },
        {
          name: 'public-subnet-1',
          subnetType: ec2.SubnetType.PUBLIC,
          cidrMask: 24,
        },
        {
          name: 'DB-subnet',
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
          cidrMask: 28,
        },
      ],
    });

    console.log(`VPC stack is ${vpc.privateSubnets[0].subnetId}`);


    // 👇 update the Name tag for the VPC
    cdk.Aspects.of(vpc).add(new cdk.Tag('Name', 'my-cdk-vpc'));

    vpc.addFlowLog('FlowlLogCloudWatch', {
      trafficType: ec2.FlowLogTrafficType.ALL
    });
  }
}


const app = new cdk.App();
new vpcStack(app, 'vpc-stack');
app.synth();