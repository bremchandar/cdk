interface Person {
    firstName: String,
    lastName: String,
    isVisitor?: Boolean
}

function generateEmail(input:Person, force?:boolean) {
    if(input.isVisitor && !force ){
        return undefined
    }else {
    return `${input.firstName}.${input.lastName}@gmail.com`
    }
}

function isPerson(potentialPerson: any): boolean{
    if('firstName' in potentialPerson &&
        'lastName' in potentialPerson){
            return true
        }
        else{
            return false
        }    
}

function printEmailIfPerson(potentialPerson:any): void{
    if (isPerson(potentialPerson)){
        console.log(generateEmail(potentialPerson))
    }else{
        console.log('Input is not a person')
    }
}

printEmailIfPerson({
    firstName: 'Brem',
    lastName: 'Chandru'
})
