#!/bin/bash
export aws_sso_start_url=https://d-976704e007.awsapps.com/start/
echo $aws_sso_start_url
echo ap-southeast-2
echo configuring jims-billing-int-admin
winpty aws configure sso --profile jims-billing-int-admin
echo configuring jims-billing-prod-admin
winpty aws configure sso --profile jims-billing-prod-admin
echo configuring jims-billing-uat-admin
winpty aws configure sso --profile jims-billing-uat-admin
echo configuring jims-hf-admin
winpty aws configure sso --profile jims-hf-admin
echo configuring jims-int-admin
winpty aws configure sso --profile jims-int-admin
echo configuring jims-master-admin
winpty aws configure sso --profile jims-master-admin
echo configuring jims-log-admin
winpty aws configure sso --profile jims-log-admin
echo configuring jims-prod-admin
winpty aws configure sso --profile jims-prod-admin
echo configuring jims-sandbox-admin
winpty aws configure sso --profile jims-sandbox-admin
echo configuring jims-uat-admin
winpty aws configure sso --profile jims-uat-admin
echo configuring jims-audit-admin
winpty aws configure sso --profile jims-audit-admin
echo jims-shared-services-admin
winpty aws configure sso --profile jims-shared-services-admin
