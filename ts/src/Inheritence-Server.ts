

abstract class BaseServer {

    protected port: number
    protected address: string

    constructor(port:number, address:string){
        this.port = port;
        this.address=address;
    
    }

    startServer(): void {
        console.log(`Starting Server at: ${this.port}: ${this.address}`)
    }

    stopServer(): void {
        console.log('Stopping Server')
    }
}

class DBServer extends BaseServer{

    constructor(port:number, address:string){
        super(port,address)
        console.log('calling db Server constructor')
    }
}
const somedbServer = new DBServer(8080,'localhost');
somedbServer.startServer()
somedbServer.stopServer()

