"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import ecs = require('aws-cdk-lib/aws-ecs');
//import ecs_patterns = require('aws-cdk-lib/aws-ecs-patterns');
const ec2 = require("aws-cdk-lib/aws-ec2");
const cdk = require("aws-cdk-lib");
//import * as logs from '@aws-cdk/aws-logs';
class vpcStack extends cdk.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);
        // Create a VPC
        const vpc = new ec2.Vpc(this, 'my-cdk-vpc', {
            cidr: '10.20.0.0/16',
            natGateways: 1,
            maxAzs: 3,
            subnetConfiguration: [
                {
                    name: 'Application-subnet',
                    subnetType: ec2.SubnetType.PRIVATE_WITH_NAT,
                    cidrMask: 24,
                },
                {
                    name: 'public-subnet-1',
                    subnetType: ec2.SubnetType.PUBLIC,
                    cidrMask: 24,
                },
                {
                    name: 'DB-subnet',
                    subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
                    cidrMask: 28,
                },
            ],
        });
        // 👇 update the Name tag for the VPC
        cdk.Aspects.of(vpc).add(new cdk.Tag('Name', 'my-cdk-vpc'));
        vpc.addFlowLog('FlowlLogCloudWatch', {
            trafficType: ec2.FlowLogTrafficType.ALL
        });
    }
}
const app = new cdk.App();
new vpcStack(app, 'vpc-stack');
app.synth();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDhDQUE4QztBQUM5QyxnRUFBZ0U7QUFDaEUsMkNBQTRDO0FBQzVDLG1DQUFvQztBQUNwQyw0Q0FBNEM7QUFLNUMsTUFBTSxRQUFTLFNBQVEsR0FBRyxDQUFDLEtBQUs7SUFDOUIsWUFBWSxLQUFjLEVBQUUsRUFBVSxFQUFFLEtBQXNCO1FBQzVELEtBQUssQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXhCLGVBQWU7UUFHZixNQUFNLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRTtZQUMxQyxJQUFJLEVBQUUsY0FBYztZQUNwQixXQUFXLEVBQUUsQ0FBQztZQUNkLE1BQU0sRUFBRSxDQUFDO1lBQ1QsbUJBQW1CLEVBQUU7Z0JBQ25CO29CQUNFLElBQUksRUFBRSxvQkFBb0I7b0JBQzFCLFVBQVUsRUFBRSxHQUFHLENBQUMsVUFBVSxDQUFDLGdCQUFnQjtvQkFDM0MsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLGlCQUFpQjtvQkFDdkIsVUFBVSxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsTUFBTTtvQkFDakMsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7Z0JBQ0Q7b0JBQ0UsSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLFVBQVUsRUFBRSxHQUFHLENBQUMsVUFBVSxDQUFDLGdCQUFnQjtvQkFDM0MsUUFBUSxFQUFFLEVBQUU7aUJBQ2I7YUFDRjtTQUNGLENBQUMsQ0FBQztRQUVILHFDQUFxQztRQUNyQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBRTNELEdBQUcsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLEVBQUU7WUFDbkMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHO1NBQ3hDLENBQUMsQ0FBQztJQUVMLENBQUM7Q0FDRjtBQUdELE1BQU0sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO0FBQzFCLElBQUksUUFBUSxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztBQUMvQixHQUFHLENBQUMsS0FBSyxFQUFFLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvL2ltcG9ydCBlY3MgPSByZXF1aXJlKCdhd3MtY2RrLWxpYi9hd3MtZWNzJyk7XG4vL2ltcG9ydCBlY3NfcGF0dGVybnMgPSByZXF1aXJlKCdhd3MtY2RrLWxpYi9hd3MtZWNzLXBhdHRlcm5zJyk7XG5pbXBvcnQgZWMyID0gcmVxdWlyZSgnYXdzLWNkay1saWIvYXdzLWVjMicpO1xuaW1wb3J0IGNkayA9IHJlcXVpcmUoJ2F3cy1jZGstbGliJyk7XG4vL2ltcG9ydCAqIGFzIGxvZ3MgZnJvbSAnQGF3cy1jZGsvYXdzLWxvZ3MnO1xuXG5cblxuXG5jbGFzcyB2cGNTdGFjayBleHRlbmRzIGNkay5TdGFjayB7XG4gIGNvbnN0cnVjdG9yKHNjb3BlOiBjZGsuQXBwLCBpZDogc3RyaW5nLCBwcm9wcz86IGNkay5TdGFja1Byb3BzKSB7XG4gICAgc3VwZXIoc2NvcGUsIGlkLCBwcm9wcyk7XG5cbiAgICAvLyBDcmVhdGUgYSBWUENcblxuICAgIFxuICAgIGNvbnN0IHZwYyA9IG5ldyBlYzIuVnBjKHRoaXMsICdteS1jZGstdnBjJywge1xuICAgICAgY2lkcjogJzEwLjIwLjAuMC8xNicsXG4gICAgICBuYXRHYXRld2F5czogMSxcbiAgICAgIG1heEF6czogMyxcbiAgICAgIHN1Ym5ldENvbmZpZ3VyYXRpb246IFtcbiAgICAgICAge1xuICAgICAgICAgIG5hbWU6ICdBcHBsaWNhdGlvbi1zdWJuZXQnLFxuICAgICAgICAgIHN1Ym5ldFR5cGU6IGVjMi5TdWJuZXRUeXBlLlBSSVZBVEVfV0lUSF9OQVQsXG4gICAgICAgICAgY2lkck1hc2s6IDI0LFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgbmFtZTogJ3B1YmxpYy1zdWJuZXQtMScsXG4gICAgICAgICAgc3VibmV0VHlwZTogZWMyLlN1Ym5ldFR5cGUuUFVCTElDLFxuICAgICAgICAgIGNpZHJNYXNrOiAyNCxcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIG5hbWU6ICdEQi1zdWJuZXQnLFxuICAgICAgICAgIHN1Ym5ldFR5cGU6IGVjMi5TdWJuZXRUeXBlLlBSSVZBVEVfSVNPTEFURUQsXG4gICAgICAgICAgY2lkck1hc2s6IDI4LFxuICAgICAgICB9LFxuICAgICAgXSxcbiAgICB9KTtcblxuICAgIC8vIPCfkYcgdXBkYXRlIHRoZSBOYW1lIHRhZyBmb3IgdGhlIFZQQ1xuICAgIGNkay5Bc3BlY3RzLm9mKHZwYykuYWRkKG5ldyBjZGsuVGFnKCdOYW1lJywgJ215LWNkay12cGMnKSk7XG5cbiAgICB2cGMuYWRkRmxvd0xvZygnRmxvd2xMb2dDbG91ZFdhdGNoJywge1xuICAgICAgdHJhZmZpY1R5cGU6IGVjMi5GbG93TG9nVHJhZmZpY1R5cGUuQUxMXG4gICAgfSk7XG4gIFxuICB9XG59XG5cblxuY29uc3QgYXBwID0gbmV3IGNkay5BcHAoKTtcbm5ldyB2cGNTdGFjayhhcHAsICd2cGMtc3RhY2snKTtcbmFwcC5zeW50aCgpOyJdfQ==