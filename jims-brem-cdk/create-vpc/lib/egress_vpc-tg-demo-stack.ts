
import cdk = require('aws-cdk-lib');

import { aws_ec2 as ec2 } from 'aws-cdk-lib';
import * as iam from 'aws-cdk-lib/aws-iam';
import { Construct } from 'constructs';



export class EgressVpcTgDemoStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props ? : cdk.StackProps) {
    super(scope, id, props);

    //set-up egress and private VPCs
    const egressVPC = new ec2.Vpc(this, 'Egress VPC', {
      cidr: "10.100.0.0/16",
      natGateways: 1, //add this to limit numer of deployed NAT gateways
      maxAzs: 2,
      subnetConfiguration: [
        {
          cidrMask:28,
          name: 'Public - EgressVPC SubNet',
          subnetType: ec2.SubnetType.PUBLIC,
        },
        {
          cidrMask: 25,
          name: 'Private - EgressVPC SubNet',
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
        },
       // {
       //   cidrMask: 26,
       //   name: 'NAT - EgressVPC SubNet',
       //   subnetType: ec2.SubnetType.PRIVATE_WITH_NAT,
       // },
      ]
    });

   //console.log(`TG vpc stack is ${egressVPC.privateSubnets[0].subnetId}`);


    const privateVPC = new ec2.Vpc(this, 'Private VPC', {
      cidr: "10.5.0.0/16",
      maxAzs: 1,
      enableDnsHostnames: true,
      enableDnsSupport: true,
      subnetConfiguration: [{
        cidrMask: 25,
        name: 'Isolated Subnet - privateVPC',
        subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
      }],
    });

    const dataVPC = new ec2.Vpc(this, 'Data VPC', {
      cidr: "10.10.0.0/16",
      maxAzs: 1,
      enableDnsHostnames: true,
      enableDnsSupport: true,
      subnetConfiguration: [{
        cidrMask: 25,
        name: 'Isolated Subnet - DataVPC',
        subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
      },
      ],
    });

    dataVPC.addFlowLog('FlowlLogCloudWatch', {
      trafficType: ec2.FlowLogTrafficType.ALL
    });
    // Security Group to be used by EC2 instances in isolated subnet and accessed via Systems Manager
    const ssmPrivateSG = new ec2.SecurityGroup(this, 'SSMPrivateSecurityGroup', {
      vpc: privateVPC,
      securityGroupName: 'Demo EC2 Instance Security Group',
      description: 'Demo EC2 Instance Security Group',
      allowAllOutbound: true,
    });

    // uncomment below lines to add private link endpoints for Systems Manager
    
    // adding interface endpoints for Systems Manger use - only 443 from EC2-SG to Interface Endpoints necessary
    // const ssmIE = privateVPC.addInterfaceEndpoint('SSM', {
    //   service: ec2.InterfaceVpcEndpointAwsService.SSM,
    //   privateDnsEnabled: true,
    //   subnets: { subnetType: ec2.SubnetType.ISOLATED, onePerAz: true },
    // });
    // ssmIE.connections.allowFrom(ssmPrivateSG, ec2.Port.tcp(443), 'Allow from SSM IE Private SG');

    // const ssmMessagesIE = privateVPC.addInterfaceEndpoint('SSM-Messages', {
    //   service: ec2.InterfaceVpcEndpointAwsService.SSM_MESSAGES,
    //   privateDnsEnabled: true,
    //   subnets: { subnetType: ec2.SubnetType.ISOLATED, onePerAz: true },
    // });
    // ssmMessagesIE.connections.allowFrom(ssmPrivateSG, ec2.Port.tcp(443), 'Allow from SSM Messages IE Private SG');

    // const ec2IE = privateVPC.addInterfaceEndpoint('EC2', {
    //   service: ec2.InterfaceVpcEndpointAwsService.EC2,
    //   privateDnsEnabled: true,
    //   subnets: { subnetType: ec2.SubnetType.ISOLATED, onePerAz: true },
    // });
    // ec2IE.connections.allowFrom(ssmPrivateSG, ec2.Port.tcp(443), 'Allow from EC2 IE Private SG');

    // const ec2Messages = privateVPC.addInterfaceEndpoint('EC2-messages', {
    //   service: ec2.InterfaceVpcEndpointAwsService.EC2_MESSAGES,
    //   privateDnsEnabled: true,
    //   subnets: { subnetType: ec2.SubnetType.ISOLATED, onePerAz: true },
    // });
    // ec2Messages.connections.allowFrom(ssmPrivateSG, ec2.Port.tcp(443), 'Allow from EC2 Messages IE Private SG');

    // privateVPC.addGatewayEndpoint('S3-SSM', {
    //   service: ec2.GatewayVpcEndpointAwsService.S3,
    //   subnets: [{ subnetType: ec2.SubnetType.ISOLATED, onePerAz: true }],
    // });
//Create TG gateway
const TransitGateway = new ec2.CfnTransitGateway(this, 'Transit_Gateway', {
  description: "Transit Gateway",
  vpnEcmpSupport: 'enable',
  defaultRouteTableAssociation: 'disable',
  defaultRouteTablePropagation: 'disable',
  tags: [{
    key: 'Name',
    value: "Transit Gateway"
  }],
});
//attach VPCs to gateway
    const subnetId = ['subnetid'];

const TransitGatewayAttachmentEgress = new ec2.CfnTransitGatewayAttachment(this, 'TransitGatewayAttachmentEgress', {
  transitGatewayId: TransitGateway.ref,
  vpcId: egressVPC.vpcId,
  //subnetIds: ['subnetid'],
  subnetIds: [egressVPC.isolatedSubnets[0].subnetId, egressVPC.isolatedSubnets[1].subnetId],
  tags: [{
    key: 'Name',
    value: "TG-Egress-VPC-Private_SubNet-Attachment"
  }],
});
TransitGatewayAttachmentEgress.addDependsOn(TransitGateway);

const TransitGatewayAttachmentPrivate = new ec2.CfnTransitGatewayAttachment(this, 'TransitGatewayAttachmentPrivate', {
  transitGatewayId: TransitGateway.ref,
  vpcId: privateVPC.vpcId,
  subnetIds: [privateVPC.isolatedSubnets[0].subnetId],
  tags: [{
    key: 'Name',
    value: "TG-Private-VPC-Private_SubNet-Attachment"
  }],
});
TransitGatewayAttachmentEgress.addDependsOn(TransitGateway);

const TransitGatewayAttachmentData= new ec2.CfnTransitGatewayAttachment(this, 'TransitGatewayAttachmentData', {
  transitGatewayId: TransitGateway.ref,
  vpcId: dataVPC.vpcId,
  subnetIds: [dataVPC.isolatedSubnets[0].subnetId],
  tags: [{
    key: 'Name',
    value: "TG-dataVPC_SubNet-Attachment"
  }],
});
TransitGatewayAttachmentEgress.addDependsOn(TransitGateway);

//add routes
for (let subnet of egressVPC.publicSubnets) {
  new ec2.CfnRoute(this, subnet.node.id, {
    routeTableId: subnet.routeTable.routeTableId,
    destinationCidrBlock: privateVPC.vpcCidrBlock,
    transitGatewayId: TransitGateway.ref,
  }).addDependsOn(TransitGatewayAttachmentEgress);
};

for (let subnet of privateVPC.isolatedSubnets) {
  new ec2.CfnRoute(this, subnet.node.id, {
    routeTableId: subnet.routeTable.routeTableId,
    destinationCidrBlock: "0.0.0.0/0",
    transitGatewayId: TransitGateway.ref,
  }).addDependsOn(TransitGatewayAttachmentPrivate);
};

for (let subnet of dataVPC.isolatedSubnets) {
  new ec2.CfnRoute(this, subnet.node.id, {
    routeTableId: subnet.routeTable.routeTableId,
    destinationCidrBlock: "0.0.0.0/0",
    transitGatewayId: TransitGateway.ref,
  }).addDependsOn(TransitGatewayAttachmentData);
};

//add TG Route Domain (fancy name for route table) and internet egress route
const TGRouteTable = new ec2.CfnTransitGatewayRouteTable(this, "TGEgressRouteTable", {
  transitGatewayId: TransitGateway.ref,
  tags: [{
    key: 'Name',
    value: "TG Route Domain"
  }],
}); 
const TransitGatewayRouteTable = new ec2.CfnTransitGatewayRoute(this, "TransitGatewayToEgressVPCRoute", {
  transitGatewayRouteTableId: TGRouteTable.ref,
  transitGatewayAttachmentId: TransitGatewayAttachmentEgress.ref,
  destinationCidrBlock: "0.0.0.0/0"
});
const TGRouteTableAssociationEgressVPC = new ec2.CfnTransitGatewayRouteTableAssociation(this, 'EgressVPC_TG_Association', {
  transitGatewayAttachmentId: TransitGatewayAttachmentEgress.ref,
  transitGatewayRouteTableId: TransitGatewayRouteTable.transitGatewayRouteTableId,
});
const TGRouteTablePropagationEgressVPC = new ec2.CfnTransitGatewayRouteTablePropagation(this, 'EgressVPC_TG_Propagation', {
  transitGatewayAttachmentId: TransitGatewayAttachmentEgress.ref,
  transitGatewayRouteTableId: TransitGatewayRouteTable.transitGatewayRouteTableId,
});
const TGRouteTableAssociationPrivateVPC = new ec2.CfnTransitGatewayRouteTableAssociation(this, 'PrivateVPC_TG_Association', {
  transitGatewayAttachmentId: TransitGatewayAttachmentPrivate.ref,
  transitGatewayRouteTableId: TransitGatewayRouteTable.transitGatewayRouteTableId,
});
const TGRouteTablePropagationPrivateVPC = new ec2.CfnTransitGatewayRouteTablePropagation(this, 'PrivateVPC_TG_Propagation', {
  transitGatewayAttachmentId: TransitGatewayAttachmentPrivate.ref,
  transitGatewayRouteTableId: TransitGatewayRouteTable.transitGatewayRouteTableId,
});
const TGRouteTableAssociationDataVPC = new ec2.CfnTransitGatewayRouteTableAssociation(this, 'DataVPC_TG_Association', {
  transitGatewayAttachmentId: TransitGatewayAttachmentData.ref,
  transitGatewayRouteTableId: TransitGatewayRouteTable.transitGatewayRouteTableId,
});
const TGRouteTablePropagationDataVPC = new ec2.CfnTransitGatewayRouteTablePropagation(this, 'DataVPC_TG_Propagation', {
  transitGatewayAttachmentId: TransitGatewayAttachmentData.ref,
  transitGatewayRouteTableId: TransitGatewayRouteTable.transitGatewayRouteTableId,
});

// Time to test the routes. 

//Getting latest Amazon Linux AMI
const latestLinuxAMI = new ec2.AmazonLinuxImage({
  generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
  edition: ec2.AmazonLinuxEdition.STANDARD,
  virtualization: ec2.AmazonLinuxVirt.HVM,
  storage: ec2.AmazonLinuxStorage.GENERAL_PURPOSE,
});
//ssm agent Role - we don't want to rely on a bastion host
const SSMRole = new iam.Role(this, 'SSMRole', {
  assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
  managedPolicies: [
    iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMManagedInstanceCore'),
    iam.ManagedPolicy.fromAwsManagedPolicyName('CloudWatchAgentServerPolicy'),
  ],
  // optional inline policy for S3 SSM
  inlinePolicies: {
    ssmS3policy: new iam.PolicyDocument({
      statements: [
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: [
            's3:GetObject'
          ],
          resources: [
            'arn:aws:s3:::aws-ssm-' + this.region +'/*',
            'arn:aws:s3:::aws-windows-downloads-' + this.region +'/*',
            'arn:aws:s3:::amazon-ssm-' +this.region+'/*',
            'arn:aws:s3:::amazon-ssm-packages-' + this.region +'/*',
            'arn:aws:s3:::' + this.region +'-birdwatcher-prod/*',
            'arn:aws:s3:::patch-baseline-snapshot-' + this.region +'/*'
          ]
        })
      ]
    })
  }
});

//Launch instance in private VPC subnet 
const demoInstance = new ec2.CfnInstance(this, "Demo Instance", {
  subnetId: privateVPC.isolatedSubnets[0].subnetId,
  imageId: latestLinuxAMI.getImage(this).imageId,
  instanceType: "t2.nano",
  iamInstanceProfile: new iam.CfnInstanceProfile(this, "DemoEC2_InstanceProfile", {
    roles: [SSMRole.roleName]
  }).ref,
  tags: [{
    key: 'Name',
    value: "Demo instance"
  }],
  securityGroupIds: [ssmPrivateSG.securityGroupId] //The Security Group we created earlier, linked to private interface endpoints
});
}
}